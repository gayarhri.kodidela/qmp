import React from "react";
import './insight.css';
import { Table } from 'react-bootstrap';
import HomeService from "../../services/HomeService";
import Common from "../../utils/Common";
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
class InsightInput extends React.Component {

    constructor() {
        super();
        this.state = {
            user: {},
            insightData: [],
            processedData: [],
            headings: [],
            loading: true,
            segmentInsightRole: false,
            segmentDataRole: false,
            corporateInsightRole: false,
            corporateDataRole: false,
            isAdmin: false,
            showSaveButton : false,
            // assignedDataMetric:[],
            // assignedInsightMetric:[],
            // assignedCorporateMetric:[],
            // assignedDataSegment:[],
            // assignedInsightSegment:[],
            // assignedCorporateSegment:[],
            segmentInsight :{},
            segmentData:{},
            corportateInsight:{},
            corportateData: {}
        }
        this.processTable = this.processTable.bind(this);
        this.setColor = this.setColor.bind(this);
        this.editPerformance = this.editPerformance.bind(this);
        this.savePerformance = this.savePerformance.bind(this);
        this.homeService = new HomeService();
        this.setLoading = this.setLoading.bind(this);
        this.commonService = new Common();
    }
    componentDidMount() {
        this.getInsightData();
        this.commonService.isAdmin().then(role =>{
            console.log("User role is admin:"+(typeof role));
            if(role === "true"){
               
              this.setState({isAdmin: true, showSaveButton: true});
            }else{
                var segmentDataRole = false;
                var segmentnsightRole = false;
                var corporateInsightRole = false;
                var corporateDataRole = false;
                var segmentInsight = {};
                var segmentData = {};
                var corporateData = {};
                var corporateInsight = {};
                this.commonService.getUser().then( user =>{
                     for(var key in user.roles){
                       if(key === "Segment Data"){
                            segmentDataRole = true;
                            segmentData = user.roles[key]
                       }
                       if(key === "Segment Insights"){
                        segmentnsightRole = true;
                        segmentInsight = user.roles[key];
                       }
                        if(key === "Corporate Insights"){
                            corporateInsightRole = true;
                            corporateInsight = user.roles[key];
                        }
                        if(key === "Corporate Data"){
                            corporateDataRole = true;
                            corporateData = user.roles[key];
                        }
                     }
                     if(segmentDataRole === true || segmentnsightRole === true || corporateInsightRole === true || corporateDataRole === true){
                         this.setState({showSaveButton: true});
                     }
                     this.setState({ segmentInsightRole: segmentnsightRole,segmentDataRole: segmentDataRole,corporateInsightRole: corporateInsightRole,
                        corporateDataRole:corporateDataRole,corportateInsight:corporateInsight,corportateData:corporateData,
                        segmentInsight:segmentInsight,segmentData:segmentData })
                  })
            }
        })
    }
    setLoading() {
        this.setState({ loading: !this.state.loading });
    }
    getInsightData() {
        console.log(this.state.user);
        this.homeService.displayMetricsinputscreen().then(item => {
            console.log(item);
            this.setLoading();
            if (item.status == 'Success') {
                this.setState({ insightData: JSON.parse(item.data) });
                console.log(this.state.insightData);
                this.processTable(this.state.insightData);
            }
        })
    }
    processTable(data) {
        var table = [...data];
        var heading = [];
        
        var today = new Date();
        var quarter = Math.floor((today.getMonth() + 3) / 3);
        console.log("currentQuater:", quarter)
        //table.forEach(data =>{
            

        for (var key in table[0]) {
            if (key !== "Metric_ID" && key !== "Year") {
                let res = key.replace(/_/g, " ");
                if(res.includes("Performance")){
                    res = key.replace("Performance","Perf").replace(/_/g," ");
                    heading.push(res);
                } else {
                    heading.push(res);
                }
                
            }
        }
        //})
        table.forEach(data => {
            var performanceData = [];
            
            //console.log(index);
            
            for (var key in data) {
                if (key !== "Metric_ID" && key !== "Year" && key !== "Category" && key !== "Leading/Lagging" && key !== "Metric_Name" && key !== "Segment" && key !== "UOM") {
                    if (key.includes(quarter)) {
                        var editable = false;
                        var editSegmentData = false;
                        var editSegmentInsight = false;
                        var editCorporateInsight = false;
                        var editCorporateData = false;
                        if(this.state.isAdmin === true){
                            editable = true;
                        }else{
                            editable = false;
                            if(this.state.segmentDataRole === true){
                                for(var insigthKey in this.state.segmentData){
                                    if(insigthKey === data.Segment){
                                       var metric =  this.state.segmentData[insigthKey];
                                       var index = metric.findIndex(e=> e === data.Metric_ID)
                                       if(index > -1){
                                        editSegmentData = true;
                                       }
                                    }
                                }
                            }
                            if(this.state.segmentInsightRole === true){
                                for(var insigthKey in this.state.segmentInsight){
                                    if(insigthKey === data.Segment){
                                       var metric =  this.state.segmentInsight[insigthKey];
                                       var index = metric.findIndex(e=> e === data.Metric_ID)
                                       if(index > -1){
                                        editSegmentInsight = true;
                                       }
                                    }
                                }                         
                             }
                             if(this.state.corporateInsightRole === true){
                                for(var insigthKey in this.state.corportateInsight){
                                    if(insigthKey === data.Segment){
                                       var metric =  this.state.corportateInsight[insigthKey];
                                       var index = metric.findIndex(e=> e === data.Metric_ID)
                                       if(index > -1){
                                        editCorporateInsight = true;
                                       }
                                    }
                                }  
                             }
                             if(this.state.corporateDataRole === true){
                                for(var insigthKey in this.state.corportateData){
                                    if(insigthKey === data.Segment){
                                       var metric =  this.state.corportateData[insigthKey];
                                       var index = metric.findIndex(e=> e === data.Metric_ID)
                                       if(index > -1){
                                        editCorporateData = true;
                                       }
                                    }
                                }  
                             }
                        }
                        performanceData.push({ value: data[key], editable: editable,editSegmentData:editSegmentData,editSegmentInsight:editSegmentInsight, editCorporateInsight:editCorporateInsight,
                            editCorporateData:editCorporateData, heading: key })
                    } else {
                        performanceData.push({ value: data[key], editable: false, heading: key })
                    }

                    delete data[key];
                }
            }
            data["performanceData"] = performanceData;
            // var index = segment.findIndex(s=> s.segment === data.Segment);
            // if(index > -1){
            //     data["color"] = segment[index].color;
            //     console.log(segment[index].color)
            // }
        })
        console.log(table);
        this.setColor(table);
        this.setState({ headings: heading, processedData: table });
    }
    setColor(table){
        var colorCodes = ['FFFAF0','FDF5E6','FFFFF0','FFF5EE','F0FFF0','F5FFFA','F5FFFA','F0FFFF','F0F8FF','F0F8FF','FFF0F5','FAFAD2','F5F5DC','F5DEB3','f5d4da','FFFACD','FFFFF0','F0FFF0','FFF0F5','F0FFFF','E0FFFF','f9d6f9']
        var segment = [];
        table.forEach(e=>{
            var index = segment.findIndex(s => s.segment === e.Segment);
            if(index === -1){
                // const hue = Math.floor(Math.random() * 360);
                // const randomColor = 'hsl('+hue+',70%,80%)'
                // var letters = 'BCDEF'.split('');
                // var color = '#';
                // for (var i = 0; i < 6; i++) {
                //     color += letters[Math.floor(Math.random() * letters.length)];
                // }
                var color = colorCodes[Math.floor(Math.random() * colorCodes.length)];
                var element = colorCodes.splice(Math.floor(Math.random() * colorCodes.length),1);
               // colorCodes[Math.floor(Math.random() * colorCodes.length)];
                segment.push({segment : e.Segment, color:"#"+color});
               // e["color"] = color
            }
        })
        table.forEach(e=>{
            var index = segment.findIndex(s=> s.segment === e.Segment);
            if(index > -1){
                e["color"] = segment[index].color;
            }
        })
        console.log(segment);
        console.log(table);
        this.setState({processedData: table });
    }
    editPerformance(target, event) {
        target.value = event.target.value;
        console.log(target);
    }
    savePerformance(target, event) {
        console.log(target)
        var targetToEdit = JSON.parse(JSON.stringify(target));
        targetToEdit.performanceData.forEach(data => {
            targetToEdit[data.heading] = data.value;
        })
        delete targetToEdit["performanceData"];
        delete targetToEdit["color"];
        console.log(targetToEdit)
        this.setLoading();
        this.homeService.editPerformance(targetToEdit).then(item => {
            this.setLoading();
            if (item.status == 'Success') {
                var data = [...this.state.insightData];
                var index = data.findIndex(d => d.Metric_ID == target.Metric_ID);
                if (index > -1) {
                    data.splice(index, 1, target);
                }
                this.setState({ processedData: data });
                //  this.processTable(data);
            }
        })
    }
    render() {
        return (
            <div>
                {this.state.loading && <div className="loader-box">
                    <div id="loader"></div>
                </div>}
                <div style={{ overflow: "auto", height: "80vh" }}>
                    <Table responsive style={{ width: "100%" }}>
                        <thead>
                            <tr>
                                {
                                    this.state.headings.map(heading => {
                                        if(heading === 'Segment'){
                                            return (<th className="fixedHeader">{heading}</th>)
                                        }else if(heading === 'Category'){
                                            return (<th className="fixedHeader" style={{left:"81px"}}>{heading}</th>)
                                        }else if(heading === 'Metric Name'){
                                            return (<th className="fixedHeader" style={{left:"178px"}}>{heading}</th>)
                                        }else if(heading === 'Leading/Lagging'){
                                            return (<th>Lead/Lag</th>)
                                        }else if(heading.includes("Insights")){
                                            return (<th style={{"width":"15%"}}>{heading}</th>)
                                        }else{ 
                                            return (<th>{heading}</th>)
                                        }
                                       
                                    })
                                }
                                {this.state.showSaveButton === true && <th></th>}
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.processedData.map(data => {
                                    return (
                                        <tr style={{background: data.color}}>
                                            <td className="fixedRow" style={{background: data.color}}>{data.Segment}</td>
                                            <td className="fixedRow" style={{left:"81px",background: data.color}}>{data.Category}</td>
                                            <td className="fixedRow" style={{left:"178px",background: data.color}}>{data.Metric_Name}</td>
                                            <td>{data["Leading/Lagging"]}</td>
                                            <td>{data.UOM}</td>
                                            {
                                                data.performanceData.map(pd => {
                                                    if (pd.editable === false) {
                                                        if(pd.editSegmentInsight === true && pd.heading.includes("Insights")){
                                                            return (<td><textarea type="text" defaultValue={pd.value} onChange={e => this.editPerformance(pd, e)} /></td>)
                                                        }else if(pd.editSegmentData === true && !pd.heading.includes("Insights")){
                                                            return (<td><input type="text" style={{"width":"80%"}} defaultValue={pd.value} onChange={e => this.editPerformance(pd, e)} /></td>)
                                                        }else if(pd.editCorporateInsight === true && pd.heading.includes("Insights")){
                                                            return (<td><textarea type="text" defaultValue={pd.value} onChange={e => this.editPerformance(pd, e)} /></td>)
                                                        }else if( pd.editCorporateData === true && !pd.heading.includes("Insights")){
                                                            return (<td><input type="text" style={{"width":"80%"}} defaultValue={pd.value} onChange={e => this.editPerformance(pd, e)} /></td>)
                                                        } else{
                                                            return(<td>{pd.value}</td>)
                                                        }
                                                    } else {
                                                        if (pd.heading.includes("Insights")) {
                                                            return (<td><textarea type="text" defaultValue={pd.value} onChange={e => this.editPerformance(pd, e)} /></td>)
                                                        } else {
                                                            return (<td><input type="text" style={{"width":"80%"}} defaultValue={pd.value} onChange={e => this.editPerformance(pd, e)} /></td>)
                                                        }

                                                    }

                                                })
                                            }
                                            { this.state.showSaveButton === true &&  <td><button onClick={e => this.savePerformance(data, e)}>Save</button></td>}
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
}

export default InsightInput;