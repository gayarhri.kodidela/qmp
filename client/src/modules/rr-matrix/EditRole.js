import React, { useState, useEffect } from "react";
import '../Popup.css';
import './EditRole.css';
import HomeService from "../../services/HomeService";
import  MultiSelect  from "react-multi-select-component";
import MetricNameDropDown from "../MetricNameDropDown";
import { unmountComponentAtNode, render } from "react-dom";

const AddNewRole = props => {
  // const [heading, setHeading] = useState(props.heading);
  const [metricDetails, setMetricDetails] = useState(props.metric);
  const [userList, setuUerList] = useState(props.userList);
  const [showSegmentOrMetric, setShowSegmentOrMetric] = useState('Metric');
  const [loading, setloading] = useState(false);
  const [segmentRole, setSegmentRole]  = useState(props.segmentRole);
  const [metricRole, setMetricRole]  = useState(props.metricRole);
  const [targetRole, setTargetRole]  = useState(props.targetRole);
  //  let showSegmentOrMetric = "Metric";


// const div =  document.querySelector('button') // Get element from DOM
// div.classList.remove('clear-selected-button')

  const homeService = new HomeService;
  useEffect(() => {
    console.log("Use effect")
    var myobj = document.querySelectorAll(".clear-selected-button");
    console.log(myobj);
    myobj.forEach(e =>{
      e.remove();
    })
      //myobj.remove(); 
  }, []);
  console.log(segmentRole);
  console.log("metricRole__",metricRole);
  console.log("targetRole___",targetRole);

  // const[role, setRole] = useState({})
  const changeInput = (value, role, section) => {
    role.value = value.target.value
    console.log(role);
    if (section === 'Segment') {
      var data = JSON.parse(JSON.stringify(metricDetails));
      for (var key in data.segment_insights) {
        if (key.includes(role.name)) {
          data.segment_insights[key] = role.value;
        }
      }
      setMetricDetails(data);
    } else if(section === 'Metric'){
      var data = JSON.parse(JSON.stringify(metricDetails));
      for (var key in data.metrics_input) {
        if (key.includes(role.name)) {
          data.metrics_input[key] = role.value;
        }
      }
      setMetricDetails(data);
    }else{
      var data = JSON.parse(JSON.stringify(metricDetails));
      for (var key in data.target_definition) {
        if (key.includes(role.name)) {
          data.target_definition[key] = role.value;
        }
      }
      setMetricDetails(data);
    }

  }
  const editMetricOrSegment = (value, option) => {
    console.log(option);
    
    
    
    setShowSegmentOrMetric(option);
    setTimeout(() => {
      var myobj = document.querySelectorAll(".clear-selected-button");
      console.log(myobj);
      if(myobj.length > 0){
        myobj.forEach(e =>{
          e.remove();
        })
      }
    }, 1000);
   
  }
  const saveRoles = () => {
    delete metricDetails["metricInput"]
    delete metricDetails["segmentInput"]
    delete metricDetails["targetDefination"]
    showLoader();
    console.log("Saving Edit roles")
    console.log(metricDetails);
    homeService.editRrmatrixRoles(metricDetails).then(item => {
      console.log(item);
      showLoader();
      let metricInput = [];
      let segmentInput = [];
      let targetDefination = [];
      for (var key in metricDetails.metrics_input) {
        metricInput.push(metricDetails.metrics_input[key])
      }
      for (var key in metricDetails.segment_insights) {
        segmentInput.push(metricDetails.segment_insights[key])
      }
      for (var key in metricDetails.target_definition) {
        targetDefination.push(metricDetails.target_definition[key])
      }
      metricDetails.metricInput = metricInput;
      metricDetails.segmentInput = segmentInput;
      metricDetails.targetDefination = targetDefination;
      props.saveEditedRoles(metricDetails);
    })
  }
  const showLoader = () => {
    setloading(!loading);
  }
  const selectedSegmentName= (name,role) =>{
    console.log("name__",name,role);
   // setSelectedValue(name);
    console.log(role);
     role.value = name;
    var sRole = [...segmentRole];
    var index = sRole.findIndex(e=> e.name === role.name);
    console.log(index);
    if(index > -1){
      sRole.splice(index,1,role);
      
    }
    setSegmentRole(sRole);
    var data = JSON.parse(JSON.stringify(metricDetails));
      for (var key in data.segment_insights) {
        if (key.includes(role.name)) {
          var nameArr = [];
          role.value.forEach(element => {
              nameArr.push(element.label);
          });
          data.segment_insights[key] = nameArr.join(";");
        }
      }
      setMetricDetails(data);
  }
  // const selectedMetricName = (name,role) =>{
  //   console.log("name__",name,role);
  //   console.log(role);
  //   role.value = name;
  //   let mRole = [...metricRole];
  //   console.log("mrole__ befire",mRole)
  //   let index = mRole.findIndex(e=> e.name === role.name);
  //   console.log("selectedMetricNameIndex",index);
  //   if(index > -1){
  //     mRole.splice(index,1,role);
      
  //   }
  //   console.log("mrole__ after",mRole)
  //   setMetricRole(mRole);
  //   var data = JSON.parse(JSON.stringify(metricDetails));
  //     for (var key in data.metrics_input) {
  //       if (key.includes(role.name)) {
  //         var nameArr = [];
  //         role.value.forEach(element => {
  //             nameArr.push(element.label);
  //         });
  //         data.metrics_input[key] = nameArr.join(";");
  //       }

  //     }
  //     setMetricDetails(data);
  // }



  const selectedMetricName = (name,role) =>{
    

    
    console.log("name__",name,role);
    console.log(role);
    
    
role.value = name;

    let mRole = [...metricRole];
    console.log("mrole__ befire",mRole)
    let index = mRole.findIndex(e=> e.name === role.name);
    console.log("selectedMetricNameIndex",index);
    
    if(index > -1){
      mRole.splice(index,1,role);
      
    }

    console.log("mrole__ after",mRole)
    setMetricRole(mRole);
    

    var data = JSON.parse(JSON.stringify(metricDetails));
      for (var key in data.metrics_input) {
        if (key.includes(role.name)) {
          var nameArr = [];
          role.value.forEach(element => {
              nameArr.push(element.label);
          });
          data.metrics_input[key] = nameArr.join(";");
        }

      }
      setMetricDetails(data);
  }

  const selectedTargetName = (name,role) =>{
    console.log(name);
    console.log(role);
    role.value = name;
    var tRole = [...targetRole];
    var index = tRole.findIndex(e=> e.name === role.name);
    console.log(index);
    if(index > -1){
      tRole.splice(index,1,role);
      
    }
    setTargetRole(tRole);
    var data = JSON.parse(JSON.stringify(metricDetails));
      for (var key in data.target_definition) {
        if (key.includes(role.name)) {
          var nameArr = [];
          role.value.forEach(element => {
              nameArr.push(element.label);
          });
          data.target_definition[key] = nameArr.join(";");
        }
      }
      setMetricDetails(data);
  }
  // const addNewRole = (value)=>{
  //    metricDetails.role = role;
  //    console.log(metricDetails);
  //    props.saveNewMetric(metricDetails);
  // }



useEffect(() => {
 
 
// const div =  document.querySelector('div') // Get element from DOM
// div.classList.remove('clear-selected-button')


//  unmountComponentAtNode(document.getElementsByClassName('clear-selected-button'));
//  unmountComponentAtNode(document.getElementsByClassName('dropdown-heading-value'));
 
}, [])





  return (
    <div className="popup-box">
      {loading && <div className="loader-box">
        <div id="loader"></div>
      </div>}
      {!loading &&
        <div className="box">
          <div style={{ padding: "15px", backgroundColor: "rgb(211, 219, 241)" }}>
            <span className="close-icon" onClick={props.handleClose}>x</span>
            <h3 style={{ margin: "0" }}>Edit Roles</h3>
          </div>
          <div>

          </div>
          <div className="modal-box">
            <label className="input-label">
              Category
              <input type="text" name="metricName" readOnly value={metricDetails.Category} />

            </label>
            <label className="input-label">
              Metric Name
              <input type="text" name="metricName" readOnly value={metricDetails["Metric Name"]} />
            </label>
            <label className="input-label">
              Persona
              <div style={{ marginTop: "1%" }}>
                <input type="radio" id="Metric" defaultChecked name="Metric_Segment" value="Metric" onChange={(value) => editMetricOrSegment(value, 'Metric')} />
                <label>Metric Input</label>
                <input type="radio" id="Segment" name="Metric_Segment" value="Segment" onChange={(value) => editMetricOrSegment(value, 'Segment')} />
                <label>Segment Insight</label>
                <input type="radio" id="Segment" name="Metric_Segment" value="Target" onChange={(value) => editMetricOrSegment(value, 'Target')} />
                <label>Target Definition</label>
              </div>
            </label>
            
            {showSegmentOrMetric == 'Segment' &&
              segmentRole.map(role => {
                return (<label className="input-label">
                  {role.name}
                  
                  {/* <input type="text" defaultValue={role.value} onChange={(value) => changeInput(value, role, 'Segment')} /> */}
                  <MultiSelect
                      options={userList}
                      value={role.value}
                      onChange={(name) => selectedSegmentName(name,role)}
                      labelledBy="Select"
                      // ClearSelectedIcon
                    />
                </label>)
              })
            }
            {showSegmentOrMetric == 'Metric' &&
              metricRole.map(role => {
                return (<label >      
                  {role.name}
                  {/* <input type="text" defaultValue={role.value} onChange={(value) => changeInput(value, role, 'Metric')} /> */}
                 {/* <pre>{JSON.stringify(role.value)}</pre> */}
                  <div>
                  <MultiSelect
                      // key={role.name}
                      options={userList}
                      value={role.value}
                      onChange={(name) => selectedMetricName(name,role)}
                      // labelledBy="Select"
                      ClearSelectedIcon
                      // style={{display:'block'}}
                    />
                    </div>
                </label>)
              })
            }
            {showSegmentOrMetric == 'Target' &&
              targetRole.map(role => {
                return (<label className="input-label">
                  {role.name}
                  {/* <input type="text" defaultValue={role.value} onChange={(value) => changeInput(value, role, 'Target')} /> */}
                  <MultiSelect
                      options={userList}
                      value={role.value}
                      onChange={(name) => selectedTargetName(name,role)}
                      labelledBy="Select"
                      // ClearSelectedIcon
                    />
                </label>)
              })
            }

            <div style={{ textAlign: "center" }}>
              <button onClick={saveRoles}>Save</button>
              <button onClick={props.handleClose}>Cancel</button>
            </div>

          </div>
        </div>
      }
    </div>
  );
};

export default AddNewRole;