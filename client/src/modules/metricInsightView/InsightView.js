import React from "react";
import '../metricInsightInput/insight.css';
import { Table } from 'react-bootstrap';
import HomeService from "../../services/HomeService";
import Common from "../../utils/Common";
import MetricNameDropDown from "../MetricNameDropDown";
import info from '../../images/info.png';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Tooltip from 'react-bootstrap/Tooltip';
class InsightView extends React.Component {

    constructor() {
        super();
        this.state = {
            insightData: [],
            insightOriginalData: [],
            loading: true,
            heading: [],
            subHeading: [],
            tableData: [],
            category: [],
            segment: [],
            metric: [],
            selectedYear: "",
            year: [],
            popUpModelOpen: false,
            selectedCategory: [],
            selectedSegment: [],
            selectedMetric: [],
            selectedType: "Product",
            changedYearOrType: false,
            isAdmin: false
        }
        this.setLoading = this.setLoading.bind(this);
        this.getColorCoding = this.getColorCoding.bind(this);
        this.hideQuater = this.hideQuater.bind(this);
        this.showQuater = this.showQuater.bind(this);
        this.filterDataByYear = this.filterDataByYear.bind(this);
        this.filterDataCategory = this.filterDataCategory.bind(this);
        this.filterDataMetric = this.filterDataMetric.bind(this);
        this.filterDataSegment = this.filterDataSegment.bind(this);
        this.homeService = new HomeService();
        this.commonSerice = new Common();
    }
    componentDidMount() {
        this.getInsightData();
        this.getYearList();
        this.commonSerice.isAdmin().then(role =>{
            console.log("User role is admin:"+role);
            if(role === "true"){
                this.setState({isAdmin: true});
            }
            
          })

    }
    getYearList() {
        this.setState({ selectedYear: new Date().getFullYear() })
        this.homeService.getYearFilter().then(item => {
            if (item.status == 'Success') {
                let data = JSON.parse(item.data);
                console.log("Year", data);
                this.setState({ ...this.state, year: data });
            }
        })
    }
    showQuater(quater, event) {
        var tableData = [...this.state.tableData]
        tableData.forEach(e => {
            e.quater.forEach(q => {
                for (var key in q) {
                    var prefKey = "";
                    if (key.includes("Performance")) {
                        prefKey = key.replace("Performance", "Perf").replace(/_/g, " ");
                    } else {
                        prefKey = key.replace(/_/g, " ");
                    }
                    if (prefKey === quater.heading) {
                        console.log(key);
                        e[key] = q[key];
                    }
                }
            })
        })
        console.log(tableData);
        var heading = [...this.state.heading];
        var headingIndex = heading.findIndex(e => e.heading === quater.heading);
        if (headingIndex > -1) {
            var obj = JSON.parse(JSON.stringify(quater));
            obj.value = obj.heading;
            heading.splice(headingIndex, 1, obj);
        }
        console.log(heading);
        this.setState({ ...this.state, tableData: tableData, heading: heading });


    }
    hideQuater(quater, event) {
        console.log(quater);
        console.log(event);
        var data = [...this.state.tableData];
        data.forEach(e => {
            for (var key in e) {
                var perf = "";
                if (key.includes("Performance")) {
                    perf = key.replace("Performance", "Perf").replace(/_/g, " ");
                } else {
                    perf = key.replace(/_/g, " ");
                }
                if (perf === quater.heading) {
                    console.log(key);
                    e[key] = "";
                }
            }
        })
        console.log(data);

        var heading = [...this.state.heading];
        var headingIndex = heading.findIndex(e => e.heading === quater.heading);

        if (headingIndex > -1) {
            var obj = JSON.parse(JSON.stringify(quater));
            obj.value = " ";
            heading.splice(headingIndex, 1, obj);
        }
        console.log(heading);
        this.setState({ ...this.state, heading: heading });
    }
    filterDataByYear(event) {
        console.log(event.target.value);
        this.setLoading();
        var year;
        var metricType;
        if (event.target.value === "Service" || event.target.value === "Product") {
            this.setState({ selectedType: event.target.value, popUpModelOpen: true });
            metricType = event.target.value
            year = this.state.selectedYear;
        } else {
            this.setState({ selectedYear: event.target.value, popUpModelOpen: true });
            metricType = this.state.selectedType
            year = event.target.value;
        }
        this.homeService.filterDataByYearInsightView(year, metricType).then(item => {
            this.setLoading();
            if (item.status == 'Success') {
                let data = JSON.parse(item.data);
                console.log("Filter data by year", data);
                this.setState({ insightData: data, popUpModelOpen: false, insightOriginalData: data });
                this.processData();
                this.getDropDownData(data);
            }
        })

    }
    filterDataCategory(category) {
        console.log(category);
        var data = [...this.state.insightOriginalData];
        var categoryFilteredData = [];
        var selectedSegment = [...this.state.selectedSegment]
        var selectedMetric = [...this.state.selectedMetric]
        var metricName = [];
        var i , j , k;
        data.forEach(e => {
            if(selectedSegment.length > 0 && selectedMetric.length > 0){
                for( i=j=k=0 ; i<category.length && j<selectedSegment.length && k<selectedMetric.length; i++,j++,k++){
                    if(e.Category === category[i].value && e.Segment === selectedSegment[j].value && e.Metric_Name === selectedMetric[k].value){
                        categoryFilteredData.push(e);
                    }
                }
            }else if(selectedSegment.length === 0 && selectedMetric.length > 0){
                for( i=j=0 ; i<category.length && j<selectedMetric.length; i++,j++){
                    if(e.Category === category[i].value && e.Metric_Name === selectedMetric[j].value){
                        categoryFilteredData.push(e);
                    }
                }
            }else if(selectedSegment.length > 0 && selectedMetric.length === 0){
                for( i=j=0 ; i<category.length || j<selectedSegment.length; i++,j++){
                    console.log("Category:"+i+":"+category[i].value)
                    console.log("Segment:"+j+":"+selectedSegment[j].value)
                    if(e.Category === category[i].value && e.Segment === selectedSegment[j].value){
                        categoryFilteredData.push(e);
                    }
                }
            }else if(selectedSegment.length === 0 && selectedMetric.length === 0){
                for( i=0 ; i<category.length; i++){
                    if(e.Category === category[i].value){
                        categoryFilteredData.push(e);
                    }
                }
            }
        })
        
        console.log("Category Filter:",categoryFilteredData)
        // console.log("Metric Filter:",metricFilteredData)
        //console.log("Final Filter:",finalArray)
        categoryFilteredData.forEach( e=>{
            const foundMetricName = metricName.find(c => c.value == e.Metric_Name);
                if (!foundMetricName) {
                // metricName.push(e.Metric_Name);
                metricName.push({ label: e.Metric_Name, value: e.Metric_Name });
             }

        })
        this.setState({ insightData: categoryFilteredData, selectedCategory: category,metric: metricName }, () => {
            console.log(this.state.insightData);
            this.processData();
        });
    }
    filterDataSegment(segments) {
        console.log(segments);
        var data = [...this.state.insightOriginalData];
        var selectedCategory = [...this.state.selectedCategory]
        var selectedMetric = [...this.state.selectedMetric]
        var segmentFilteredData = [];
        var category = [];
        var metricName = [];
        var i , j , k;
        data.forEach(e => {
            if(selectedCategory.length > 0 && selectedMetric.length > 0){
                for( i=j=k=0 ; i<segments.length && j<selectedCategory.length && k<selectedMetric.length; i++,j++,k++){
                    if(e.Segment === segments[i].value && e.Category === selectedCategory[j].value && e.Metric_Name === selectedMetric[k].value){
                        segmentFilteredData.push(e);
                    }
                }
            }else if(selectedCategory.length === 0 && selectedMetric.length > 0){
                for( i=j=0 ; i<segments.length && j<selectedMetric.length; i++,j++){
                    if(e.Segment === segments[i].value && e.Metric_Name === selectedMetric[k].value){
                        segmentFilteredData.push(e);
                    }
                }
            }else if(selectedCategory.length > 0 && selectedMetric.length === 0){
                for( i=j=0 ; i<segments.length && j<selectedCategory.length; i++,j++){
                    if(e.Segment === segments[i].value && e.Category === selectedCategory[j].value){
                        segmentFilteredData.push(e);
                    }
                }
            }else if(selectedCategory.length === 0 && selectedMetric.length === 0){
                for( i=0 ; i<segments.length; i++){
                    console.log(segments[i].value)
                    if(e.Segment === segments[i].value){
                        segmentFilteredData.push(e);
                    }
                }
            }
        })
        console.log("Segment Filter:",segmentFilteredData)
        segmentFilteredData.forEach( e=>{
            const found = category.find(c => c.value === e.Category);
            if (!found) {
                category.push({ label: e.Category, value: e.Category });
            }

            const foundMetricName = metricName.find(c => c.value == e.Metric_Name);
                if (!foundMetricName) {
                // metricName.push(e.Metric_Name);
                metricName.push({ label: e.Metric_Name, value: e.Metric_Name });
             }

        })
        this.setState({ insightData: segmentFilteredData, selectedSegment: segments, category: category,metric: metricName  }, () => {
           // console.log(this.state.insightData);
            this.processData();
        });
    }
    filterDataMetric(metrics) {
        console.log(metrics);
        var data = [...this.state.insightData];
        var insgthData = [];
        data.forEach(e => {
            metrics.forEach(metric => {
                if (e.Metric_Name === metric.value) {
                    insgthData.push(e);
                }
            })
        })
        this.setState({ insightData: insgthData, selectedMetric: metrics }, () => {
            console.log(this.state.insightData);
            this.processData();
        });
    }
    processData() {
        var data = [...this.state.insightData]
        var heading = [];
        data.forEach(e => {
            var arr = [];
            for (var key in e) {
                if (key.includes("Q1") || key.includes("Q2") || key.includes("Q3") || key.includes("Q4")) {
                    var obj = {};
                    obj[key] = e[key];
                    arr.push(obj);
                }
            }
            e["quater"] = [...arr];
        })
        console.log(data);
        for (var key in data[0]) {
            if (key != "quater" && key != "Metric_ID" && key != "Year" && key != "is_disabled" && key != "Product/Service" && !key.includes("color") && key != "allowEdit") {
                if (key.includes("Performance")) {
                    var head = key.replace("Performance", "Perf").replace(/_/g, " ");
                    if(head === "Perf Target")
                    {
                        heading.push({heading: "Target", value: "Target"})
                    } else{
                        heading.push({ heading: head, value: head });
                    }
                    
                } else {
                    heading.push({ heading: key.replace(/_/g, " "), value: key.replace(/_/g, " ") });
                }

            }
        }
        console.log(this.state.insightData);
        this.setState({ ...this.state, heading: heading, tableData: data }, () => {
            console.log(this.state.tableData);
            this.getColorCoding();
        });

    }
    getDropDownData(data) {
        const category = [];
        const segment = [];
        const metricName = [];

        data.forEach(e => {
            // if (!category.includes(e.Category)) {
            const found = category.find(c => c.value === e.Category);
            if (!found) {
                category.push({ label: e.Category, value: e.Category });
            }

            //}
            //if (!segment.includes(e.Segment)) {
            const foundSegment = segment.find(c => c.value === e.Segment);
            if (!foundSegment) {
                segment.push({ label: e.Segment, value: e.Segment });
            }

            //}
           // if (!metricName.includes(e.Metric_Name)) {
                 const foundMetricName = metricName.find(c => c.value === e.Metric_Name);
                if (!foundMetricName) {
                metricName.push({ label: e.Metric_Name, value: e.Metric_Name });
                }
                  
            //}
        })
       // console.log(metricName)
       segment.sort(function(a, b) {
            var textA = a.value.toUpperCase();
            var textB = b.value.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        metricName.sort(function(a, b) {
            var textA = a.value.toUpperCase();
            var textB = b.value.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        category.sort(function(a, b) {
            var textA = a.value.toUpperCase();
            var textB = b.value.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        this.setState({ ...this.state, category: category, segment: segment, metric: metricName });
    }
    getColorCoding() {
        var colorCodes = ['FFFAF0','FDF5E6','FFFFF0','FFF5EE','F0FFF0','F5FFFA','F5FFFA','F0FFFF','F0F8FF','F0F8FF','FFF0F5','FAFAD2','F5F5DC','F5DEB3','f5d4da','FFFACD','FFFFF0','F0FFF0','FFF0F5','F0FFFF','E0FFFF','f9d6f9']
        var data = [...this.state.tableData];
        data.forEach(e => {
            var performance = parseInt(e.Performance_Target);
            var previousYearTarget = 0;
           // var color = colorCodes[Math.floor(Math.random() * colorCodes.length)];
            //var element = colorCodes.splice(Math.floor(Math.random() * colorCodes.length),1);

            e.quater.forEach(q => {
                if (q["Previous_Year_Q4_Performance"]) {
                    previousYearTarget = parseInt(q["Previous_Year_Q4_Performance"]);
                }
            })
            e.quater.forEach(q => {
                if (q["Q1_Performance"]) {
                    var q1Performace = parseInt(q["Q1_Performance"]);
                    //console.log(q["Q1"]);
                    //console.log(q1Performace+">="+performance);
                    if (performance == 0) {
                        e["Q1_color"] = "No target";
                    } else if (isNaN(performance)) {
                        e["Q1_color"] = "No data";
                    } else if (q1Performace >= performance) {
                        e["Q1_color"] = "green";
                    } else if (q1Performace >= previousYearTarget) {
                        e["Q1_color"] = "yellow";
                    } else if (q1Performace <= previousYearTarget) {
                        e["Q1_color"] = "red";
                    }
                } else if (q["Q2_Performance"]) {
                    var q1Performace = parseInt(q["Q2_Performance"]);
                    //console.log(q1Performace+">="+performance);
                    if (performance == 0) {
                        e["Q2_color"] = "No target";
                    } else if (isNaN(performance)) {
                        e["Q2_color"] = "No data";
                    } else if (q1Performace >= performance) {
                        e["Q2_color"] = "green";
                    } else if (q1Performace >= previousYearTarget) {
                        e["Q2_color"] = "yellow";
                    } else if (q1Performace <= previousYearTarget) {
                        e["Q2_color"] = "red";
                    }
                } else if (q["Q3_Performance"]) {
                    var q1Performace = parseInt(q["Q3_Performance"]);
                    //console.log(q1Performace+">="+performance);
                    if (performance == 0) {
                        e["Q3_color"] = "No target";
                    } else if (isNaN(performance)) {
                        e["Q3_color"] = "No data";
                    } else if (q1Performace >= performance) {
                        e["Q3_color"] = "green";
                    } else if (q1Performace >= previousYearTarget) {
                        e["Q3_color"] = "yellow";
                    } else if (q1Performace <= previousYearTarget) {
                        e["Q3_color"] = "red";
                    }
                } else if (q["Q4_Performance"]) {
                    var q1Performace = parseInt(q["Q4_Performance"]);
                    console.log("Q4:", q["Q4_Performance"]);
                    if (performance == 0) {
                        e["Q4_color"] = "No target";
                    } else if (isNaN(performance)) {
                        e["Q4_color"] = "No data";
                    } else if (q1Performace >= performance) {
                        console.log("Green:", q1Performace >= performance);
                        e["Q4_color"] = "green";
                    } else if (q1Performace >= previousYearTarget) {
                        e["Q4_color"] = "yellow";
                    } else if (q1Performace <= previousYearTarget) {
                        e["Q4_color"] = "red";
                    }
                }


            })
        })
       
        var metric = [];
        data.forEach(e=>{
            var index = metric.findIndex(s => s.segment === e.Segment);
            if(index === -1){
                var color = colorCodes[Math.floor(Math.random() * colorCodes.length)];
                var element = colorCodes.splice(Math.floor(Math.random() * colorCodes.length),1);
                metric.push({segment : e.Segment, color:"#"+color});
            }
        })
        data.forEach(e=>{
            var index = metric.findIndex(s=> s.segment === e.Segment);
            if(index > -1){
                e["color"] = metric[index].color;
            }
        })
        console.log("Inside Get colors");
        console.log(data);
       
        this.setState({ ...this.state, tableData: data });
    }
    setLoading() {
        this.setState({ loading: !this.state.loading });
    }
    getInsightData() {
        var year = new Date().getFullYear();
        this.homeService.filterDataByYearInsightView(year, this.state.selectedType).then(item => {
            this.setLoading();
            if (item.status == 'Success') {
                let data = JSON.parse(item.data);
                console.log("Filter data by year", data);
                data.sort((a,b) => (a.Segment > b.Segment) ? 1 : ((b.Segment > a.Segment) ? -1 : 0))
                this.setState({ insightData: data, insightOriginalData: data });
                this.processData();
                this.getDropDownData(data);
            }
        })

        // this.homeService.displayMetricsViewScreen().then(item => {
        //     this.setLoading();
        //     if (item.status == 'Success') {
        //         this.setState({ insightData: JSON.parse(item.data), insightOriginalData: JSON.parse(item.data) });
        //         console.log(JSON.parse(item.data))
        //         this.processData();
        //         this.getDropDownData(JSON.parse(item.data))
        //     }
        // })
    }
    editPerformance(target, event, key) {
        target[key] = event.target.value;
        console.log(target);
    }
    savePerformance(target, event) {
        var targetData = JSON.parse(JSON.stringify(target));
        console.log(targetData);
        delete targetData["quater"];
        delete targetData["is_disabled"];
        //targetData.forEach(e=>{
        for (var key in targetData) {
            if (key.includes("color")) {
                delete targetData[key];
            }
        }
        //})
        this.setState({ popUpModelOpen: true });
        this.setLoading();
        this.homeService.editMetricInsightPerformance(targetData).then(item => {
            this.setLoading();
            this.setState({ popUpModelOpen: false });
            if (item.status === 'Success') {
                var data = [...this.state.insightData];
                var index = data.findIndex(d => d.Metric_ID == targetData.Metric_ID);
                if (index > -1) {
                    data.splice(index, 1, target);
                }
                this.processData();
                //var arr = [];
                // for (var key in targetData) {
                //     if (key.includes("Q1") || key.includes("Q2") || key.includes("Q3") || key.includes("Q4")) {
                //         var obj = {};
                //         obj[key] = targetData[key];
                //         arr.push(obj);
                //     }
                // }
                // targetData["quater"] = [...arr];

                // this.setState({ ...this.state, tableData: data}, () => {
                //     console.log("Inside Edit performance");
                //     console.log(this.state.insightData);
                //     this.getColorCoding();
                // });
            }
        })
    }
    render() {
        // const targetInput={
        //     width:"60%"
        // }
        return (
            <div>
                {this.state.loading && <div className="loader-box">
                    <div id="loader"></div>
                </div>}
                
                <div className="filter" style={{ "marginBottom": "1%" }}>
                    <div className="filterDiv" style={{ verticalAlign: "top" }}>
                        <label style={{ display: "block", paddingRight: "0" }}>Year</label>
                        <select className="filterDropDown" onChange={this.filterDataByYear} value={this.state.selectedYear}>
                            {
                                this.state.year.map((y) => {
                                    return (<option key={y} value={y}>{y}</option>)
                                })
                            }
                        </select>
                    </div>
                    <div className="filterDiv" style={{ verticalAlign: "top" }}>
                        <label style={{ display: "block", paddingRight: "0" }}>Metric Type</label>
                        <select className="filterDropDown" onChange={this.filterDataByYear} value={this.state.selectedType}>
                            <option value="Product">Product</option>
                            <option value="Service">Service</option>
                        </select>
                    </div>
                    <div className="filterDiv" style={{ width: "20%" }}>
                        <label>Segment</label>
                        <MetricNameDropDown metricNameList={this.state.segment}
                            resetSelectedMetric={this.state.changedYearOrType}
                            filterMetricByName={this.filterDataSegment.bind(this)} ></MetricNameDropDown>
                    </div>
                    <div className="filterDiv" style={{ width: "20%" }}>
                        <label>Category</label>
                        <MetricNameDropDown metricNameList={this.state.category}
                            resetSelectedMetric={this.state.changedYearOrType}
                            filterMetricByName={this.filterDataCategory.bind(this)} ></MetricNameDropDown>
                        {/* <select className="filterDropDown" onChange={this.filterDataCategory} value={this.state.selectedCategory}>
                            <option value="" disabled selected hidden>Select Category</option>
                            {
                                this.state.category.map(name => {
                                    return (<option key={name} value={name}>{name}</option>)
                                })
                            }
                        </select> */}
                    </div>

                    <div className="filterDiv" style={{ width: "20%" }}>
                        <label>Metric</label>
                        <MetricNameDropDown metricNameList={this.state.metric}
                            resetSelectedMetric={this.state.changedYearOrType}
                            filterMetricByName={this.filterDataMetric.bind(this)} ></MetricNameDropDown>
                        
                    </div>
                    <OverlayTrigger
                key="mainMenu"
                placement="left"
                overlay={
                  <Tooltip id="tooltip-metricList" className="colorCode-tooltip">
                   <table style={{width:"50%", float: "right"}}>
                    <tbody>
                        <tr>
                            <td style={{backgroundColor: "#a6db00", color:"white"}}><b>Target Reached</b></td>
                            <td style={{backgroundColor: "#ff3c14", color:"white"}}><b>Target not Reached and worst than previous YE perf</b></td>
                            <td style={{backgroundColor: "#ff8200"}}><b>Target not Reached but better than previous YE perf</b></td>
                            <td style={{backgroundColor: "#f4f1f1"}}><b>No Target determined</b></td>
                            <td style={{backgroundColor: "#aba9a9", width: "12%"}}><b>No data</b></td>
                        </tr>
                    </tbody>
                    </table>
                  </Tooltip>
                }
              >
                <img style={{width: "2%"}} src={info} />
              </OverlayTrigger>
                       
                </div>
                <div style={{ overflow: "auto", height: "70vh" }}>
                    <Table responsive style={{ width: "100%" }} id="tblMain">
                        <thead>
                            <tr>
                                {this.state.heading.map((v) => {
                                    if (v.value === 'Segment' || v.value === 'Category' || v.value === 'Metric Name' || v.value === 'Leading\/Lagging' || v.value === 'UOM' || v.value === 'Performance Target') {
                                        if (v.value === 'Segment') {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }} className="fixedHeader">{v.value}</th>)
                                        } else if (v.value === 'Category') {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : { left: "76px" }) }} className="fixedHeader">{v.value}</th>)
                                        } else if (v.value === 'Metric Name') {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : { left: "160px" }) }} className="fixedHeader">{v.value}</th>)
                                        } else if (v.value === 'Leading/Lagging') {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }}>Lead/Lag</th>)
                                        } else {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }}>{v.value}</th>)
                                        }
                                        //return (<th>{v.value}</th>)
                                    } else {
                                        if (v.value === " ") {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit", padding: "0" } : { padding: "0" }) }}>
                                                <button onClick={e => this.showQuater(v, e)}>+</button>{v.value}
                                            </th>)
                                        } else {
                                            return (<th style={{ ...(this.state.popUpModelOpen ? { position: "inherit" } : {}) }}>
                                                <button onClick={e => this.hideQuater(v, e)}>-</button>{v.value}
                                            </th>)
                                        }
                                    }
                                })}
                               { this.state.isAdmin === true && <th></th>}
                            </tr>
                        </thead>
                        <tbody>

                            {
                                this.state.insightData.map(data => {
                                    return (
                                        <tr  style={{background: data.color}}>
                                            <td className={!this.state.popUpModelOpen ? "fixedRow" : ""}  style={{background: data.color}}>{data.Segment}</td>
                                            <td className={!this.state.popUpModelOpen ? "fixedRow" : ""} style={{ left: "76px", background: data.color }}>{data.Category}</td>
                                            <td className={!this.state.popUpModelOpen ? "fixedRow" : ""} style={{ left: "160px",background: data.color }}>{data.Metric_Name}</td>
                                            <td>{data["Leading/Lagging"]}</td>
                                            <td>{data.UOM}</td>
                                            <td>{data.Performance_Target}</td>
                                            <td>
                                                {data.Previous_Year_Q4_Performance !== "" && this.state.isAdmin === true && <input type="text" defaultValue={data.Previous_Year_Q4_Performance} onChange={e => this.editPerformance(data, e, "Previous_Year_Q4_Performance")} />}
                                                 {data.Previous_Year_Q4_Performance !== "" && this.state.isAdmin === false  && <span> {data.Previous_Year_Q4_Performance} </span>} 
                                                {data.Previous_Year_Q4_Performance === "" && <span>-</span>}
                                            </td>
                                            <td>
                                                {data.Previous_Year_Q4_Insights !== "" && this.state.isAdmin === true  && <textarea type="text" defaultValue={data.Previous_Year_Q4_Insights} onChange={e => this.editPerformance(data, e, "Previous_Year_Q4_Insights")} />}
                                                {data.Previous_Year_Q4_Insights !== "" && this.state.isAdmin === false  && <span>{data.Previous_Year_Q4_Insights} </span>}
                                                
                                                {data.Previous_Year_Q4_Insights === "" && <span>-</span>}
                                            </td>
                                            <td style={{
                                                ...(data["Q1_color"] === "yellow" ? { background: "#ff8200" } : {}),
                                                ...(data["Q1_color"] === "red" ? { background: "#ff3c14" } : {}),
                                                ...(data["Q1_color"] === "green" ? { background: "#a6db00" } : {}),
                                                ...(data["Q1_color"] === "No target" ? { background: "#f9f7f7" } : {}),
                                                ...(data["Q1_color"] === "No data" ? { background: "#aba9a9" } : {})
                                            }}>
                                                {data.Q1_Performance !== "" && this.state.isAdmin === true  && <input type="text" defaultValue={data.Q1_Performance} onChange={e => this.editPerformance(data, e, "Q1_Performance")} />}
                                                {data.Q1_Performance !== "" && this.state.isAdmin === false  && <span>{data.Q1_Performance}</span>}
                                                {data.Q1_Performance === "" && <span>-</span>}
                                            </td>
                                            <td>
                                                {data.Q1_Insights !== "" && this.state.isAdmin === true  && <textarea type="text" defaultValue={data.Q1_Insights} onChange={e => this.editPerformance(data, e, "Q1_Insights")} />}
                                                {data.Q1_Insights !== "" && this.state.isAdmin === false  && <span>{data.Q1_Insights}</span>}
                                                {data.Q1_Insights === "" && <span>-</span>}
                                            </td>
                                            <td style={{
                                                ...(data["Q2_color"] === "yellow" ? { background: "#ff8200" } : {}),
                                                ...(data["Q2_color"] === "red" ? { background: "#ff3c14" } : {}),
                                                ...(data["Q2_color"] === "green" ? { background: "#a6db00" } : {}),
                                                ...(data["Q2_color"] === "No target" ? { background: "#f9f7f7" } : {}),
                                                ...(data["Q2_color"] === "No data" ? { background: "#aba9a9" } : {})
                                            }}>
                                                {data.Q2_Performance !== "" && this.state.isAdmin === true  && <input type="text" defaultValue={data.Q2_Performance} onChange={e => this.editPerformance(data, e, "Q2_Performance")} />}
                                                {data.Q2_Performance !== "" && this.state.isAdmin === false  && <span>{data.Q2_Performance} </span>}
                                                {data.Q2_Performance === "" && <span>-</span>}
                                            </td>
                                            {/* <input type="text" defaultValue={data.Q2_Performance} onChange={e => this.editPerformance(data, e,"Q2_Performance")} /></td> */}
                                            <td>
                                                {data.Q2_Insights !== "" && this.state.isAdmin === true  && <textarea type="text" defaultValue={data.Q2_Insights} onChange={e => this.editPerformance(data, e, "Q2_Insights")} />}
                                                {data.Q2_Insights !== "" && this.state.isAdmin === false  && <span>{data.Q2_Insights}</span>}
                                                {data.Q2_Insights === "" && <span>-</span>}
                                            </td>
                                            <td style={{
                                                ...(data["Q3_color"] === "yellow" ? { background: "#ff8200" } : {}),
                                                ...(data["Q3_color"] === "red" ? { background: "#ff3c14" } : {}),
                                                ...(data["Q3_color"] === "green" ? { background: "#a6db00" } : {}),
                                                ...(data["Q3_color"] === "No target" ? { background: "#f9f7f7" } : {}),
                                                ...(data["Q3_color"] === "No data" ? { background: "#aba9a9" } : {})
                                            }}>
                                                {data.Q3_Performance !== "" && this.state.isAdmin === true  &&  <input type="text" defaultValue={data.Q3_Performance} onChange={e => this.editPerformance(data, e, "Q3_Performance")} />}
                                                {data.Q3_Performance !== "" && this.state.isAdmin === false  && <span>{data.Q3_Performance}</span>}
                                                {data.Q3_Performance === "" && <span>-</span>}
                                            </td>
                                            <td>{data.Q3_Insights !== "" && this.state.isAdmin === true  && <textarea type="text" defaultValue={data.Q3_Insights} onChange={e => this.editPerformance(data, e, "Q3_Insights")} />}
                                            {data.Q3_Insights !== "" && this.state.isAdmin === false  && <span>{data.Q3_Insights}</span>}
                                                
                                                {data.Q3_Insights === "" && <span>-</span>}</td>
                                            <td style={{
                                                ...(data["Q4_color"] === "yellow" ? { background: "#ff8200" } : {}),
                                                ...(data["Q4_color"] === "red" ? { background: "#ff3c14" } : {}),
                                                ...(data["Q4_color"] === "green" ? { background: "#a6db00" } : {}),
                                                ...(data["Q4_color"] === "No target" ? { background: "#f9f7f7" } : {}),
                                                ...(data["Q4_color"] === "No data" ? { background: "#aba9a9" } : {})
                                            }}>
                                                {data.Q4_Performance !== "" && this.state.isAdmin === true && <input type="text" defaultValue={data.Q4_Performance} onChange={e => this.editPerformance(data, e, "Q4_Performance")} />}
                                                {data.Q4_Performance !== "" && this.state.isAdmin === false  && <span>{data.Q4_Performance}</span>}
                                                
                                                {data.Q4_Performance === "" && <span>-</span>}</td>
                                            <td>{data.Q4_Insights !== "" && this.state.isAdmin === true  && <textarea type="text" defaultValue={data.Q4_Insights} onChange={e => this.editPerformance(data, e, "Q4_Insights")} />}
                                            {data.Q4_Insights !== "" && this.state.isAdmin === false  &&  <span>{data.Q4_Insights}</span>}
                                                
                                                {data.Q4_Insights === "" && <span>-</span>}</td>
                                                { this.state.isAdmin === true  && <td><button onClick={e => this.savePerformance(data, e)}>Save</button></td> }

                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </Table>
                </div>
            </div>
        )
    }
}

export default InsightView;