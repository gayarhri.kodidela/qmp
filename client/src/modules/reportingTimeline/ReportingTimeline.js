import React from "react";
import { Table } from "react-bootstrap";
import HomeService from "../../services/HomeService";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Common from "../../utils/Common";
class ReportingTimeline extends React.Component {
    constructor() {
        super();
        this.state = {
            timeLineData: [],
            loading: true,
            quaterToEdit:"",
            isAdmin: false
        }
        this.setLoading = this.setLoading.bind(this);
        this.homeService = new HomeService();
        // this.getDateAbbrevation = this.getDateAbbrevation.bind(this);
        //this.setStartDate = this.setStartDate.bind(this);
        this.editReportingDueDate = this.editReportingDueDate.bind(this);
        this.commonSerice = new Common();
    }
    componentDidMount() {
        this.getTimelineData();
        this.commonSerice.isAdmin().then(role =>{
            console.log("User role is admin:"+role);
            if(role === "true"){
                this.setState({isAdmin: true});
            }
          })
    }
    getTimelineData(){
        var month = ['Jan','Feb','March','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'];
        this.homeService.displayTimelineData().then(item =>{
            console.log(item);
            if(item.status === 'Success'){
                var data = JSON.parse(item.data);
                this.setLoading();
                var today = new Date();
                var quarter = Math.floor((today.getMonth() + 3) / 3);
                var quaterToEdit = "";
                console.log("currentQuater:", quarter)
                if(this.state.isAdmin === true){
                    for(var i=quarter; i <= 4; i++){
                        quaterToEdit=quaterToEdit+""+i;
                    }
                }
                
                console.log(quaterToEdit);
                data.forEach(e=>{
                    var CYQ1 = new Date(e["CYQ1 Data Due Date"]);
                    var CYQ2 = new Date(e["CYQ2 Data Due Date"]);
                    var CYQ3 = new Date(e["CYQ3 Data Due Date"]);
                    var CYQ4 = new Date(e["CYQ4 Data Due Date"]);
                    var PYQ4 = new Date(e["PYQ4 Data Due Date"]);
                    e["Q4Date"] = new Date(e["CYQ4 Data Due Date"]);
                    e["Q3Date"] = new Date(e["CYQ3 Data Due Date"]);
                    e["Q2Date"] = new Date(e["CYQ2 Data Due Date"]);
                    e["Q1Date"] = new Date(e["CYQ1 Data Due Date"]);
                    e["PQ4Date"] = new Date(e["PYQ4 Data Due Date"]); 
                    e["CYQ1"] = month[CYQ1.getMonth()]+","+CYQ1.getDate();
                    e["CYQ2"] = month[CYQ2.getMonth()]+","+CYQ2.getDate();
                    e["CYQ3"] = month[CYQ3.getMonth()]+","+CYQ3.getDate();
                    e["CYQ4"] = month[CYQ4.getMonth()]+","+CYQ4.getDate();
                    e["PYQ4"] = month[PYQ4.getMonth()]+","+PYQ4.getDate();

                })
                this.setState({timeLineData: data, quaterToEdit: quaterToEdit});
                console.log(this.state.timeLineData);
                
            }
        })
    }
    setStartDate(event,item,quater){
        console.log("event"+event);
        console.log("item"+item);
        console.log("quater"+quater)
        var date = new Date(quater);
        if(item === "Q1"){
            event.Q1Date = quater;
            event["CYQ1 Data Due Date"] = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
        }
        else if(item === "Q2"){
            event.Q2Date = quater;
            event["CYQ2 Data Due Date"] = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
        }
        else if(item === "Q3"){
            event.Q3Date = quater;
            event["CYQ3 Data Due Date"] = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
        }
        else if(item === "Q4"){
            event.Q4Date = quater;
            event["CYQ4 Data Due Date"] = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
        }
        console.log(event);
        var timeLineData = [...this.state.timeLineData];
        var index = timeLineData.findIndex(e=> e.step_id === event.step_id);
        if(index > -1){
            timeLineData.splice(index,1,event);
        }

        this.setState({timeLineData: timeLineData});
    }
    setLoading() {
        this.setState({ loading: !this.state.loading });
    }
    // getDateAbbrevation(date){
    //     var lastDigit = date % 10;
    //     if(date >= 11 && date <= 19){
    //         return 'th';
    //     }else if(lastDigit === 1){
    //         return 'st';
    //     }else if(lastDigit === 2){
    //         return 'nd';
    //     }else if(lastDigit === 3){
    //         return 'rd';
    //     }else{
    //         return 'th';
    //     }
    // }
    editReportingDueDate(target, event) {
        target.reporting_due_date = event.target.value;
        console.log(target);
    }
    saveTimeLine(data,event){
        var editedData = JSON.parse(JSON.stringify(data));
        delete editedData.CYQ1;
        delete editedData.CYQ2;
        delete editedData.CYQ3;
        delete editedData.CYQ4;
        delete editedData.PYQ4;
        delete editedData["Q4Date"];
        delete editedData["Q3Date"];
        delete editedData["Q2Date"];
        delete editedData["Q1Date"];
        delete editedData["PQ4Date"]; 
        this.setLoading();
        var month = ['Jan','Feb','March','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'];
        this.homeService.editTimeline(editedData).then(item =>{
            this.setLoading();
            if(item.status == "Success"){
                var data = JSON.parse(item.data);
                console.log(data);
                    var CYQ1 = new Date(data["CYQ1 Data Due Date"]);
                    var CYQ2 = new Date(data["CYQ2 Data Due Date"]);
                    var CYQ3 = new Date(data["CYQ3 Data Due Date"]);
                    var CYQ4 = new Date(data["CYQ4 Data Due Date"]);
                    var PYQ4 = new Date(data["PYQ4 Data Due Date"]);
                    data["Q4Date"] = new Date(data["CYQ4 Data Due Date"]);
                    data["Q3Date"] = new Date(data["CYQ3 Data Due Date"]);
                    data["Q2Date"] = new Date(data["CYQ2 Data Due Date"]);
                    data["Q1Date"] = new Date(data["CYQ1 Data Due Date"]);
                    data["PQ4Date"] = new Date(data["PYQ4 Data Due Date"]);
                    data["CYQ1"] = month[CYQ1.getMonth()]+","+CYQ1.getDate();
                    data["CYQ2"] = month[CYQ2.getMonth()]+","+CYQ2.getDate();
                    data["CYQ3"] = month[CYQ3.getMonth()]+","+CYQ3.getDate();
                    data["CYQ4"] = month[CYQ4.getMonth()]+","+CYQ4.getDate();
                    data["PYQ4"] = month[PYQ4.getMonth()]+","+PYQ4.getDate();
                    var tableData = [...this.state.timeLineData]
                    var index = tableData.findIndex(e => e.step_id === data.step_id);
                    console.log(index);
                    if(index > -1){
                        tableData.splice(index,1,data);
                    }
                    this.setState({timeLineData: tableData});
            }
        })
    }
    render(){
        return (
            <div>
                {this.state.loading && <div className="loader-box">
                    <div id="loader"></div>
                </div>}
                <div style={{ overflow: "auto", height: "80vh" }}>
                <Table>
                <thead>
                    <tr>
                        <th>Steps</th>
                        <th>R&R</th>
                        <th>Reporting Due Date</th>
                        <th>PYQ4 Data Due Date</th>
                        <th>CYQ1 Data Due Date</th>
                        <th>CYQ2 Data Due Date</th>
                        <th>CYQ3 Data Due Date</th>
                        <th>CYQ4 Data Due Date</th>
                        {this.state.isAdmin === true && <th></th>}
                    </tr>
                </thead>
                <tbody>
                    {
                        this.state.timeLineData.map( item =>{
                            return( <tr>
                                <td>{item.steps}</td>
                                <td>{item.rr_role}</td>
                                <td>
                                   { this.state.isAdmin === true && <input type="text" defaultValue={item.reporting_due_date} onChange={e => this.editReportingDueDate(item, e)}/>} 
                                   { this.state.isAdmin === false && <span>{item.reporting_due_date}</span>} 
                                </td>
                                <td>{item.PYQ4}</td>

                                { !this.state.quaterToEdit.includes("1") &&  <td>{item.CYQ1}</td>}
                                { this.state.quaterToEdit.includes("1") && <td><DatePicker selected={item.Q4Date} onChange={this.setStartDate.bind(this,item,"Q1")} 
                                dateFormat="MMMM d, yyyy"/></td>}
                                { !this.state.quaterToEdit.includes("2") &&  <td>{item.CYQ2}</td>}
                                { this.state.quaterToEdit.includes("2") &&  <td><DatePicker selected={item.Q3Date} onChange={this.setStartDate.bind(this,item,"Q2")} 
                                dateFormat="MMMM d, yyyy"/></td>}
                                { !this.state.quaterToEdit.includes("3") &&  <td>{item.CYQ3}</td>}
                                { this.state.quaterToEdit.includes("3") &&  <td><DatePicker selected={item.Q3Date} onChange={this.setStartDate.bind(this,item,"Q3")} 
                                dateFormat="MMMM d, yyyy"/></td>}
                                { !this.state.quaterToEdit.includes("4") &&  <td>{item.CYQ4}</td>}
                                { this.state.quaterToEdit.includes("4") &&  <td><DatePicker selected={item.Q4Date} onChange={this.setStartDate.bind(this,item,"Q4")} 
                                dateFormat="MMMM d, yyyy"/></td>}
                                {/* <td>{item.CYQ4}</td> */}
                                {this.state.isAdmin === true && <td><button onClick= {e => this.saveTimeLine(item,e)}>Save</button></td>}
                        
                            </tr>)
                        })
                    }
                    
                </tbody> 
            </Table>
                </div>

            </div>
            
        )
    }

}
export default ReportingTimeline;