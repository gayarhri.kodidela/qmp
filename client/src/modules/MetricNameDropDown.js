import React, {useState, useEffect} from "react";
import  MultiSelect  from "react-multi-select-component";

const MetricNameDropDown = props => {
    const [metricName, setMetricName ] = useState(props.metricNameList);
    const [resetSelectedMetric, setResetSelectedMetric ] = useState(props.resetSelectedMetric);
    const [selectedMetricName, setSelectedMetricName] = useState([]);

    useEffect(() => {
        console.log("Use effect")
        setMetricName(props.metricNameList)
        setResetSelectedMetric(props.resetSelectedMetric)
        if(resetSelectedMetric === true){
            setSelectedMetricName([]);
        }
      }, [props.metricNameList,props.resetSelectedMetric]);


    const selectedMetric = (metricNameparams) =>{
           
            console.log("MetricNameDropDown__")
            setSelectedMetricName(metricNameparams);
            props.filterMetricByName(metricNameparams);
    }

console.log("selectedMetricName__",selectedMetricName)
    return (
        <MultiSelect
                options={metricName}
                 value={selectedMetricName}
                onChange={(name) => selectedMetric(name)}
                
                labelledBy="Select"
              />
    )

}

export default MetricNameDropDown;