import React ,  {useState} from "react";
import './Popup.css';
const DeleteSegment = props => {
    const [segment, setSegment ] = useState(props.segment);
    const [segmentToDelete, setSegmentToDelete] = useState({segment:'',comment:''});
    const change = (value,opt) => {
      if(opt == 'Segment'){
          segmentToDelete.segment = value.target.value
      }else{
        segmentToDelete.comment = value.target.value
      }
        console.log(segmentToDelete);
    }
    const handleDelete = (value)=>{
      console.log(value);
      console.log(segmentToDelete);
      props.deleteSeg(segmentToDelete);
    }
  return (
    <div className="popup-box">
      <div className="box">
          <div style={{padding: "15px", backgroundColor : "#8080cf"}}>
             <span className="close-icon" onClick={props.handleClose}>x</span>
             <h3 style={{margin: "0"}}>Delete Segment</h3>
          </div>
        <div className="modal-box">
        <label className="input-label">
              Select Segment
              <select onChange={(value)=> change(value,'Segment')}>
                <option value="Select">Select Segment</option>
                {
                  segment.map( e =>{
                    return (
                      <option key={e} value={e}>{e}</option>
                  )
                  })
                  
                }
              </select>
              {/* <input type="text" name="Category" /> */}
            </label>
            <label className="input-label">
              Comment
              <input type="text" name="comment" onChange={(value)=> change(value,'Comment')} />
            </label>
            <div style={{textAlign: "center", margin: "5%"}}>
            <button onClick={(value)=> handleDelete(value)}>Delete</button>
            <button onClick={props.handleClose}>Cancel</button>
            </div>
            
          </div>
      </div>
    </div>
  );
};
 
export default DeleteSegment;