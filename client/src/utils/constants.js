export const constants = {
    access_token: 'mars.access_token',
    refresh_token: 'mars.refresh_token',
    username: 'mars.username'
}